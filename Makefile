INSTALL ?= install

src_dir := .
build_dir := $(BUILD)
out_dir := $(OUT)

cmake_build_dir := $(build_dir)/cmake-build
config_json := $(build_dir)/config.json

.PHONY: none
none:

$(build_dir) $(cmake_build_dir):
	mkdir -p $@

.PHONY: configure
configure: $(virt_dts) | $(cmake_build_dir)
	cmake \
		-G Ninja \
		-DCROSS_COMPILER_PREFIX=$(CROSS_COMPILER_PREFIX) \
		-DCMAKE_TOOLCHAIN_FILE=$(abspath $(src_dir)/kernel/gcc.cmake) \
		-DICECAP_HACK_DTS=$(abspath $(ICECAP_HACK_DTS)) \
		-DLOCAL_SETTINGS=$(abspath $(LOCAL_SETTINGS)) \
		-DIMAGE_START_ADDR=$(IMAGE_START_ADDR) \
		-C $(abspath $(src_dir))/settings.cmake \
		-S $(abspath $(src_dir)) \
		-B $(cmake_build_dir)

.PHONY: build
build:
	ninja -C $(cmake_build_dir) elfloader kernel.elf sel4

###

.PHONY: post-build
post-build: $(config_json)

config_prefixes := \
    Kernel \
    LibSel4 \
    HardwareDebugAPI

$(config_json):
	sed -n 's,^\([A-Za-z0-9][^:]*\):\([^=]*\)=\(.*\)$$,\1:\2=\3,p' $(cmake_build_dir)/CMakeCache.txt \
		| grep -e '$$.^' $(addprefix -e ^,$(config_prefixes)) \
		| sort \
		| python3 $(src_dir)/generate_config_json.py \
		> $@

###

.PHONY: install
install: \
	install-headers \
	install-gen-src \
	$(out_dir)/lib/libsel4.a \
	$(out_dir)/boot/kernel.elf \
	$(out_dir)/boot/kernel.dtb \
	$(out_dir)/boot/loader.elf \
	$(out_dir)/sel4-aux/config.json \
	$(out_dir)/sel4-aux/platform_gen.yaml \
	$(out_dir)/sel4-aux/platform_info.h

install_cmd = $(INSTALL) -D $< $@

$(out_dir)/lib/libsel4.a: $(cmake_build_dir)/libsel4/libsel4.a
	$(install_cmd)

$(out_dir)/boot/kernel.elf: $(cmake_build_dir)/kernel/kernel.elf
	$(install_cmd)

$(out_dir)/boot/kernel.dtb: $(cmake_build_dir)/kernel/kernel.dtb
	$(install_cmd)

$(out_dir)/boot/loader.elf: $(cmake_build_dir)/elfloader-tool/elfloader
	$(install_cmd)

$(out_dir)/sel4-aux/config.json: $(config_json)
	$(install_cmd)

$(out_dir)/sel4-aux/platform_gen.yaml: $(cmake_build_dir)/kernel/gen_headers/plat/machine/platform_gen.yaml
	$(install_cmd)

$(out_dir)/sel4-aux/platform_info.h: $(cmake_build_dir)/elfloader-tool/gen_headers/platform_info.h
	$(install_cmd)

get_config_value = $(shell jq -r ".$(1) | .value" $(config_json))

# HACK
kernel_word_size = $(shell sed -nr 's,^KernelWordSize:INTERNAL=(.*)$$,\1,p' $(cmake_build_dir)/CMakeCache.txt)

libsel4_include_paths = \
      $(src_dir)/kernel/libsel4/include \
      $(src_dir)/kernel/libsel4/arch_include/$(call get_config_value,KernelArch) \
      $(src_dir)/kernel/libsel4/sel4_arch_include/$(call get_config_value,KernelSel4Arch) \
      $(src_dir)/kernel/libsel4/sel4_plat_include/$(call get_config_value,KernelPlatform) \
      $(src_dir)/kernel/libsel4/mode_include/$(kernel_word_size) \
      $(cmake_build_dir)/libsel4/include \
      $(cmake_build_dir)/libsel4/arch_include/$(call get_config_value,KernelArch) \
      $(cmake_build_dir)/libsel4/sel4_arch_include/$(call get_config_value,KernelSel4Arch) \
      $(cmake_build_dir)/kernel/gen_config \
      $(cmake_build_dir)/libsel4/gen_config \
      $(cmake_build_dir)/libsel4/autoconf

.PHONY: install-headers
install-headers:
	mkdir -p $(out_dir)/include
	for p in $(libsel4_include_paths); do \
		rsync -a $$p/ $(out_dir)/include/; \
	done

.PHONY: install-gen-src
install-gen-src:
	root=$(out_dir)/debug-aux/src && \
	d=$$root/kernel && \
		mkdir -p $$d && \
		rsync -a \
			$(cmake_build_dir)/kernel/gen* $(cmake_build_dir)/kernel/autoconf \
			$$d/ && \
	d=$$root/libsel4 && \
		mkdir -p $$d && \
		rsync -a \
			$(cmake_build_dir)/libsel4/gen* $(cmake_build_dir)/libsel4/autoconf $(cmake_build_dir)/libsel4/*include \
			$$d/
