import re
import sys
import json

r = re.compile('(?P<key>[^:]+)(:(?P<type>[^:]+))?=(?P<value>[^\\n]*)\\n')

o = {}

for line in sys.stdin:
    m = r.fullmatch(line)
    assert m is not None
    ty = m['type']
    if ty != 'INTERNAL':
        o[m['key']] = {
            'type': ty,
            'value': m['value'],
            }

json.dump(o, sys.stdout, sort_keys=True, indent=4)
